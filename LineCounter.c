#pragma config(Sensor, S1,     touchSensor,    sensorEV3_Touch)
#pragma config(Sensor, S2,     gyroSensor,     sensorEV3_Gyro, modeEV3Gyro_RateAndAngle)
#pragma config(Sensor, S3,     colorSensor,    sensorEV3_Color)
#pragma config(Sensor, S4,     sonarSensor,    sensorEV3_Ultrasonic)
#pragma config(Motor,  motorA,          leftMotor,     tmotorEV3_Large, PIDControl, encoder)
#pragma config(Motor,  motorC,          armMotor,      tmotorEV3_Large, PIDControl, driveRight, encoder)
#pragma config(Motor,  motorD,          rightMotor,    tmotorEV3_Large, PIDControl, encoder)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//


/*
this program designed to count line that it detects using light sensor. it should pause for 500ms when it detects a black line when it 1cm thick
and it should move slowly over the line after it does it add 1 to the count and update it on the display.
******************************************************

Written by: Yutaphong Gitchamnan and Patric Garland
Date: 06/03/19
Compiler: ROBOTC
Language: C
lAB: 5+6
Name: LineCounter.c

version: 1
*******************************************************/

task main()
{
	//declaring variables
	bool x = true;
	int counter = 0;
	
	
	//start while loop
	while (true)
	{
		//when color detects black
		if (SensorValue(colorSensor) > 60)
		{
			setMotorSpeed(rightMotor, 25);
			setMotorSpeed(leftMotor, 25);
			x = true;
		}//end if
		
		else
		{
			if (x == true)
			{
				counter++;
			}//end if
			setMotorSpeed(rightMotor, 0);
			setMotorSpeed(leftMotor, 0);
			sleep(500);
			
			x = false;
			setMotorSpeed(rightMotor, 25);
			setMotorSpeed(leftMotor, 25);
			sleep(300);
		}//end else
		
		eraseDisplay();
		displayBigTextLine(5, "%d", counter);
	}//end while
}//end main
